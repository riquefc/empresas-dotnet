# Desafio Pessoa Desenvolvedora .NET

## Observações

- Não estava conseguindo criar o fork na conta anterior, portanto havia subido o projeto no meu github ( https://github.com/henriqfc/ImdbAPI ), por isso a demora... 

## ImdbAPI

- API construída em .Net Core

## Autenticação

- Foi feita uma autenticação no padrão JWT, com token a ser recebido no formato Bearer. 
- Ao testar a API coloquei as funções de cadastro de Usuário e Administrador para retornar o token cadastrado e assim utilizá-los para testar as funções que necessitam de autorização (Para votar, necessita ser usuário não administrador e para cadastrar filmes necessita ser administrador).

## ORM

- Entity Framework Core

## Bancos de Dados

- SQL Server

## Code First

- Assim que o sistema se inicia ele confere se o banco já existe, caso não exista ele cria um banco com as configurações passadas inicialmente pelas classes. 

## Documentação

- Swagger utilizado. Para acessar basta ir para o link 'https://localhost:44335/swagger/index.html'

## Paginação

- Nas funções com paginação, basta enviar na query a paginação desejada.

- Por exemplo: 'https://localhost:44335/api/Filme/listar?PageNumber=2&PageSize=10'

- No caso acima, a API irá "pular" os 10 primeiros itens e trazer os próximos 10.


## 🏗 O que fazer?

- Você deve realizar um *fork* deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um *Pull Request* para isso, nós iremos avaliar e retornar por e-mail o resultado do teste.

# 🚨 Requisitos

- A API deve ser construída em .NET Core (prioritariamente) ou .NET Framework
- Implementar autenticação e deverá seguir o padrão ***JWT***, lembrando que o token a ser recebido deverá ser no formato ***Bearer***
- Implementar operações no banco de dados utilizando um ***ORM*** ou ***Micro ORM***
- **ORM's/Micro ORM's permitidos:**
    - *Entity Framework Core*
    - *Dapper*
- **Bancos relacionais permitidos**
    - *SQL Server* (prioritariamente)
    - *MySQL*
    - *PostgreSQL*
- As entidades da sua API deverão ser criadas utilizando ***Code First***. Portanto, as ***Migrations*** para geração das tabelas também deverá ser enviada no teste.
- Sua API deverá seguir os padrões REST na construção das rotas e retornos
- Sua API deverá conter documentação viva utilizando ***Swagger***
- Caso haja alguma particularidade de implementação, instruções para execução do projeto deverão ser enviadas

# 🎁 Extra

Estes itens não são obrigatórios, porém desejados.

- Testes unitários
- Teste de integração da API em linguagem de sua preferência (damos importância para pirâmide de testes)
- Cobertura de testes utilizando Sonarqube
- Utilização de *Docker* (enviar todos os arquivos e instruções necessárias para execução do projeto)

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do projeto
- Utilização de código limpo e princípios **SOLID**
- Segurança da API, como autenticação, senhas salvas no banco, *SQL Injection* e outros.
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção  [O que desenvolver?](##--o-que-desenvolver)

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deverá conter as seguintes funcionalidades:

- Administrador
    - Cadastro
    - Edição
    - Exclusão lógica (desativação)
    - Listagem de usuários não administradores ativos
        - Opção de trazer registros paginados
        - Retornar usuários por ordem alfabética
- Usuário
    - Cadastro
    - Edição
    - Exclusão lógica (desativação)
- Filmes
    - Cadastro (somente um usuário administrador poderá realizar esse cadastro)
    - Voto (a contagem de votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
    - Listagem
        - Opção de filtros por diretor, nome, gênero e/ou atores
        - Opção de trazer registros paginados
        - Retornar a lista ordenada por filmes mais votados e por ordem alfabética
    - Detalhes do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.:** 

**Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é um usuário administrador ou não.**

**Caso não consiga concluir todos os itens propostos, é importante que nos envie a implementação até onde foi possível para que possamos avaliar**